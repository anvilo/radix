<?php

// ******************************* INFORMATION ***************************//

// ***********************************************************************//
//
// ** Radix - Intelligent localization. Effortless integration.
// ** @author   Vladimir Resetnikov, vladimir@resetnikov.com
// ** @date 2017/08/20
// ** @return   $language - dynamic variable for independent processing, $langPack - for JSON
//
// ***********************************************************************//


if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

class Radix
{

    private $name = "Radix";
    private $version = 3.0;

    //{DEFAULTS}

    //basic
    private $environment = 'dev';
    private $useGET = false;
    private $getAlias = 'lg';
    private $cookieLifeTime = 31;
    private $cookieName = 'LMC';
    private $mo = null;

    //pathing

    private $chmod = 0777;

    private $jsonPath = 'lang/';
    private $logPath = 'logs/';

    private $logFile = 'radix_log.txt';

    //required parameters
    private $requiredParamaters = array('mo', 'languages', 'default_language', 'file_path');
    //optional parameters
    private $optionalParameters = array ('use_get_handle', 'get_alias', 'use_json', 'json_path', 'log_errors', 'log_path', 'log_file_name', 'cookie_life_time', 'reroute');

    //user passed settings:
    private $settings = array();

    private $languages = [];
    private $defaultLang;


    //Products of runtime
    private $language;
    public $langPack;
    private $report = array();

    private $settingsVerified = false;

    //connection
    private $url = "";

    //program meta
    private $meta = "";


// =======================================================================//
// ! Utility Functions                                                    //
// =======================================================================//

    //[SETTERS & GETTERS]

    /**
     * Language setter / getter
     *
     * @param string $language;
     *
     * @return string
     */

    public function getSetLanguage(string $language = null) {
        if ($language == null) {
            return $this -> language;
        }

        else {
            $this -> language = $language;
        }
    }

    /**
     * Language pack setter / getter
     *
     * @param string $language;
     *
     * @return mixed
     */

    public function getSetLangPack(string $language = null) {

        if ($language == null) {
            return $this -> langPack;
        }

        else {
            $path = $this -> getSetPath(null, 'json');
            $langPack = $this -> getJSON($path, $language);

            if ($langPack == true) {
                $this -> langPack = $langPack;
                return true;
            }
            else {
                return false;
            }
        }

    }

    /**
     * Default language setter / getter
     *
     * @param string $language;
     *
     * @return string
     */

    public function getSetDefaultLang(string $language = null) {

        if ($language == null) {
            return $this -> defaultLang;
        }

        else {
            $this -> defaultLang = $language;
        }

    }

    public function getSetLanguages(array $languages = null){
        if ($languages == null) {
            return $this -> languages;
        }

        else {
            $this -> languages = $languages;
        }
    }


    public function getSetCookieLifetime(int $days = null) {

        if ($days == null) {
            return $this -> cookieLifeTime;
        }

        else {
            $this -> cookieLifeTime = $days;
        }

    }

    /**
     * Get / set if $_GET method is to be used
     *
     * @param bool $value
     *
     * @return bool
     */

    public function getVariable(bool $value = false) {

        if ($value == false) {
            return $this -> useGET;
        }

        else {
            $this -> useGET = $value;
        }
    }

    /**
     * Get default $_GET alias or set a new one. Setting a new one overwrites the default one
     *
     * @param string $value
     *
     * @return string
     */


    public function getAlias(string $value = null) {

        if ($value == null) {
            return $this -> getAlias;
        }

        else {
            $this -> getAlias = $value;
        }
    }

    /**
     * Get or set JSON path
     *
     * @param string $path
     *
     * @return string
     */


    private function getSetJsonPath(string $path = null) {

        if ($path == null) {
            return $this -> $jsonPath;
        }

        else {
            $this -> jsonPath = $path;
        }
    }

    /**
 * Get or set LOG/ JSON path
 *
 * @param string $path, string $handle
 *
 * @return string
 */

    public function getSetPath(string $path = null, string $handle = 'log')
    {

        if ($path == null) {

            switch ($handle) {

                case 'log':
                    $path = $this->logPath;
                    break;

                case 'json':
                    $path = $this->jsonPath;
                    break;

            }

            if (!file_exists($path)) {

                var_dump($path);

                if (mkdir($path, true)) {

                    $this->reportHandler('3000', array('JSON parse mechanism,', $path));

                } else {
                    $this->reportHandler('1007', array('JSON parse mechanism', $path));
                    return false;
                }


            }

            return $path;

        }


        else {

            if ($handle == 'log') {
                $this -> logPath = $path;
            }

            else {
                $this -> jsonPath = $path;
            }

        }



    }

    /**
     * Get or set LOG file name
     *
     * @param string $filename
     *
     * @return string
     */

    public function getSetFilename(string $filename = null) {

        if ($filename == null) {
            return $this -> logFile;
        }

        else {
            $this -> logFile = $filename;
        }


    }

    /**
     * Check if there are optional params
     *
     * @return bool
     */

    private function checkOptionals(){

        $settings = $this -> settings();
        $parameters = $this -> getParameters();

        if (count($settings) > count($parameters)) {
            return true;
        }

        else {
            return false;
        }
    }




    /**
     * Errors storage. Dynamic - for errors that require printing dynamic results
     *
     * @param int $errorCode, $dynamic (default null)
     *
     * @return mixed
     */

    private function errors(int $errorCode, $dynamic = null) {

        $dynamicVar = $dynamic[0];
        $moreDetails = null;


        if ($dynamic != null) {
            if (count($dynamic) > 1) {
                $moreDetails = $dynamic[1];
            }
        }


        $errors = array (

            // 1000 -> critical, 2000 - warnings, 3000 - notices
            "1000" => "Radix failed to start. Please make sure you create a compatible SETTINGS array for Radix to use",
            "1001" => "Radix failed to start. Required parameter '{$dynamicVar}' was not found.",
            "1002" => "Clutter detected: Listed parameter '{$dynamicVar}' is not supported by the current Radix version.",
            "1003" => "The setting '{$dynamicVar}' is either too long or too short in character size.",
            "1004" => "The setting '{$dynamicVar}' must be a {$moreDetails}.",
            "1005" => "Invalid setting set for ('{$dynamicVar}').",
            "1006" => "{$dynamicVar} setting requires another setting to work: {$moreDetails}",
            "1007" => "The {$dynamicVar} could not find the indicated path {$moreDetails} and failed when trying to create it.",
            "1008" => "The logging mechanism failed writing data into a document '{$dynamicVar}'.",
            "1009" => "Radix failed to retrieve the requested JSON language file: {$dynamicVar}'.",
            "1010" => "Failed to establish a connection to the servers.",

            //Warnings
            "2000" => "You have attempted to set the 'get_alias' but the 'use_get_handle' is disabled. Please enable the 'use_get_handle' first.",
            "2001" => "{$dynamicVar} requires a path set. Since no path was set, a default path will be used.",
            "2002" => "Setting 'JSON path' requires an existing directory. Since no such directory was found, it will be created during the runtime of Radix if/ when needed.",
            "2003" => "Both 'reroute' and 'use_get_handle' are set to be used. Radix will always prioritize the 'use_get_handle'.",

            //Notices
            "3000" => "The {$dynamicVar} could not find the indicated path {$moreDetails} and created it. The permissions were set to 0777."



        );

        if(in_array($errors[$errorCode], $errors)) {
            $prefix = '';
            $delimiter = ':';
            switch ($errorCode) {
                case $errorCode < 2000:
                    $prefix = 'Error ';
                    break;
                case $errorCode < 3000:
                    $prefix = 'Warning ';
                    break;
                    case $errorCode >= 3000;
                    $prefix = 'Notice ';
            }
            $message = $prefix."#".$errorCode.$delimiter.' '.$errors[$errorCode];
            return $message;
        }

        else {
            return false;
        }
    }

    //[END]

    // [SETTERS]
    public function languageSetter(string $language)
    {
        $this -> language = $language;
    }

    public function lgPackSetter(string $lang_pack)
    {
        $this -> lang_pack = $lang_pack;
    }

    protected function setLanguage(string $lang)
    {
        $languages = $this -> getSetLanguages();
        if (in_array(strtoupper($lang), $this -> languages)) {
            return $lang;
        } else {
            return $this -> defaultLang;
        }
    }





    //END


    //OTHER

    private function getMeta()
    {
        $output = '';
        $data = @json_decode(file_get_contents($this -> meta));
        if ($data) {

            if ($data -> VERSION != null) {
                if ($this -> version < $data -> VERSION) {
                    $output = "There is a new version available " . ($data -> VERSION);

                } else {
                    $output = "Your RadiX version is up to date " . ($data -> VERSION);
                }

            }
        } else {
            $output = "<span class='underlineWarning'>Connection to the update server failed.</span>";
        }

        return $output;

    }




    public function getIp()
    {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = @$_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    public function getHost()
    {
        if (isset($_SERVER['SERVER_NAME'])) {
            $host = $_SERVER['SERVER_NAME'];
        } elseif (isset($_SERVER['HTTP_HOST'])) {
            $host = $_SERVER['HTTP_HOST'];
        } else {
            return false;
        }

        return $host;

    }

    public function getJSON(string $path, string $filename)
    {
        $filename = strtoupper($filename);
        $path_full = $path . $filename . ".json";
        if (file_exists($path_full)) {
            $contents = file_get_contents($path_full);
        } else {
            $this -> reportHandler(1009, array($path_full));
            return false;
        }
        if ($contents = json_decode($contents, true)) {
            return $contents;

        } else {
            return false;
        }

    }

    public function getTimeDate(string $handle) {
        $timestamp = '';
        $hours =  date("h:i:sa");
        $days = date ("Y-m-d");

        switch ($handle) {
            case 'time':
                //return time only
                $timestamp = $hours;
                break;
            case 'date':
                //return date only
                $timestamp = $days;
                break;
            case 'datetime':
                //return both
                $timestamp = $days ." ". $hours;
                break;
            default:
                //TEST IT, wrong error code
                $this -> reportHandler("1000");

        }

        return $timestamp;

    }

    public function getSetChmod(int $chmod = null) {

        if ($chmod == null) {

            return $this -> chmod;

        }

        else {
            $this -> chmod = $chmod;
        }

    }

// =======================================================================//
// ! Mainframe                                                            //
// =======================================================================//


    private function getDetails(string $ip)
    {
        $data = @json_decode(file_get_contents($this -> url . $ip));
        if ($data) {

            if ($data -> LANGUAGE != null) {
                return $data -> LANGUAGE;
            } else {
                return $this -> defaultLang;
            }

        } else {
            $this -> reportHandler("1010");
        }
    }

    private function getHeaders()
    {
        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {

            preg_match_all('/([a-z]{1,8}(-[a-z]{1,8})?)\s*(;\s*q\s*=\s*(1|0\.[0-9]+))?/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'], $lang_parse);

            if (count($lang_parse[1])) {

                $langs = array_combine($lang_parse[1], $lang_parse[4]);

                foreach ($langs as $lang => $val) {
                    if ($val === '') {

                        $langs[$lang] = 1;
                    }
                }

                $flipArray = array_flip($langs);
                $topQ = max(array_keys($flipArray));
                $topFind = $flipArray[$topQ];

                if (strpos($topFind, '-') !== false) {
                    $topFind = strtoupper(substr($topFind, strpos($topFind, '-') + 1));
                }

                return $topFind;


            } else {
                return false;
            }
        } else {
            return false;
        }

    }



    /**
     * Report Handler with set/ get features
     *
     * @param int $errorCode (default null), $dynamic (default null)
     *
     * @return mixed
     */

    public function reportHandler(int $errorCode = null, array $dynamic = null)
    {

        //check if function received no variable
        if ($errorCode == null) {
            //check if there are no errors already stored
            if(empty($this -> report)) {
                //return false if no errors located (bool)
                return false;
            }
            //else return the errors array (getter)
            else {
                return $this -> report;
            }
        }
        //if function received an error variable, update the private array $report() (setter)
        else {
            if ($dynamic == null) {
                $this -> report[] = $this -> errors($errorCode);
            }
            else {
                $this -> report[] = $this -> errors($errorCode, $dynamic);
            }
        }

    }


    protected function getMechanism(string $getName, int $cookieLifeTime)
    {

        $lang = $_GET[$getName];


        $this -> language = $this -> setLanguage($lang);

        $this -> setLangCookie($cookieLifeTime);

        //$this -> getSetLangPack($this->getSetLanguage());
    }


    /**
     * Url fixer when the $_GET is used. Also converts urls into full URLs.
     *
     * @param string $url (default null), string $handle (default null)
     *
     * @return string $url
     */

    public function fixUrl(bool $spawnHandle = false, string $url = null, string $handle = null, bool $ssl = false) {




        if ($url == null) {
            $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }

        else {


            $protocol = "http://";

            if ($ssl == true) {

                if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {

                    $protocol = "https://";
                }
            }

            if (strpos($url, $protocol) === false) {

                $url = $protocol.$url;
            }


        }

        if ($handle == null) {
            $handle = $this -> getAlias();
        }

        if (isset($_GET[$handle])) {
            $url = strtok($url, '?');
        }

        if ($spawnHandle == true) {
            $url .= "?".$handle."=";
        }

        return $url;

    }


    protected function reroute($host, string $language)
    {
        $language = strtolower($language);
        $partition = $_SERVER['REQUEST_URI'];
        $urlPiece = explode('/', $partition);
        if (!in_array($language, $urlPiece)) {
            $url = $host . "/" . $language . rtrim($partition, '/');

            $path = 'http://' . $this -> getHost() . '/' . $language;
            if (filter_var('http://' . $url, FILTER_VALIDATE_URL)) {
                header('Location: http://' . $url);
                exit;
            } else {
                $this -> reportHandler("1000");
                return false;
            }
        }

    }

    public function retrieveErrors(int $level = null)
    {

        $errors = $this -> reportHandler();

        if ($errors == false) {
            echo "No errors were found";

        } else {

            if ($level != null) {

                if ($level > 3) {
                    $level = 0;
                }

            } else {
                $level = 0;
            }


            foreach ($errors as $error) {

                $error = explode(':', $error);
                $errorPrefix = explode('#', $error[0]);
                $reportPref = strtolower($errorPrefix[0]);
                $errorCode = $errorPrefix[1];
                $url = "http://website.com/troubleshooting.php?error_id=".$errorCode;
                $link = "For more information regarding this {$reportPref} please <a href='".$url."'>visit our error database</a>.<br>";

                $errorLength = strlen($errorCode) - 1;

                $errorCode = substr($errorCode, 0, -$errorLength);

                $errorLine = '<b>' . $error[0] . ':</b>' . $error[1] . '<br>'.$link;

                if ($level == 0) {
                    echo $errorLine;
                }

                else {
                    if ($errorCode == $level) {
                        echo $errorLine;
                    }
                }


            }

        }
    }

    public function checkCookie() {

        $cookieName = $this -> cookieName;

        if (isset($_COOKIE[$cookieName])) {
            return $_COOKIE[$cookieName];
        }

        else {
            return false;
        }

    }

    public function checkUrlModifiers(array $settings) {


        if ($settings['use_get_handle'] == true) {

            if (isset($settings['get_alias'])) {
                return true;
            }

            else {

            }

        }

        else {

        }


    }


    protected function setLangCookie(int $cookieLifeTime)
    {

        setcookie('LMC', $this->language, time() + (86400 * $cookieLifeTime), "/");

    }


    /**
     * Getter/Setter for the settings
     *
     * @param array $rules (default empty)
     *
     * @return array
     */

    public function settings(array $settings = array(), string $needle = null) {

        if (empty($settings)) {
            return $this -> settings;
        }

        else {
            if ($needle == null) {
                $this -> settings = $settings;
            }

            else {
                return $this -> settings[$needle];
            }
        }

    }

    /**
     * Get parameters, default = '1' - critical params
     *
     * @param $parameterType;
     *
     * @return array
     *
     */

    private function getParameters(int $parameterType = 1) {

        switch($parameterType) {
            case 1:
                return $this -> requiredParamaters;
                break;
            case 2:
                return $this -> optionalParameters;
                break;

        }

    }


    /**
     * Verify the user provided settings
     *
     *
     * @return $errors
     */

    public function verifySettings()
    {

        $settings = $this -> settings();
        $parameters = $this -> getParameters();
        $errorCount = 0;


        //check if settings parameter is provided at all
        if (!is_array($settings) OR empty($settings)) {
            $this -> reportHandler("1000");
            return false;
        }

        $keys = array_keys($settings);
        //check if required parameters are present
        foreach ($parameters as $parameter) {
            if (!in_array($parameter, $keys)) {
                $this -> reportHandler("1001", array($parameter));
                return false;
            }
        }


        $mo = $settings['mo'];

        if ($mo != 'headers') {
            if ($mo != 'remote') {
                $this -> reportHandler("1005", array('mo'));
                $errorCount++;
            }

        }

        if (!is_string($settings['languages'])) {
            $this -> reportHandler("1004", array('languages', "STRING"));
            $errorCount++;
        }

        if (is_string($settings['default_language']) AND !is_numeric($settings['default_language'])) {
            if (strlen($settings['default_language']) > 2) {
                $this -> reportHandler("1003", array('default_language'));
            }
        } else {
            $this -> reportHandler("1004", array('default_language', "STRING"));
            $errorCount++;
        }


        //check if there are optional parameters

        if ($this -> checkOptionals() == true) {

            $usesGet = false;

            $optionalParameters = $this -> getParameters(2);

            foreach ($optionalParameters as $optionalParameter) {
                if (in_array($optionalParameter, $keys)) {

                    switch ($optionalParameter) {

                        case 'use_get_handle':

                            $getHandle = $settings['use_get_handle'];

                            if (!is_bool($getHandle)) {
                                $this -> reportHandler("1004", array('use_get_handle', "BOOLEAN"));
                                $errorCount++;
                            }

                            if ($getHandle == true) {
                                $usesGet = true;
                            }

                            break;

                        case 'get_alias':

                            $alias = $settings['get_alias'];

                            if (isset($settings['use_get_handle'])) {

                                $getHandle = $settings['use_get_handle'];

                            }

                            else {
                                $getHandle = $this -> getVariable();
                            }

                            if ($getHandle == true) {

                                if (!is_string($alias) OR is_numeric($alias)) {
                                    $this -> reportHandler("1004", array("get_alias", "STRING"));
                                    $errorCount++;
                                }

                            }

                            else {
                                $this -> reportHandler("2000");
                            }
                            break;

                        case 'use_json':

                            if (!is_bool($settings['use_json'])) {
                                $this -> reportHandler("1004", array("use_json", "BOOL"));
                                $errorCount++;
                            }

                            if (!isset($settings['json_path']) OR empty($settings['json_path'])) {
                                $this -> reportHandler("2001", array('json'));
                            }

                            else {
                                if (!file_exists($settings['json_path'])){
                                    $this -> reportHandler("2002");
                                }
                            }
                            break;


                        case 'cookie_life_time':
                            $clt = $settings['cookie_life_time'];
                            if(!is_numeric($clt)) {
                                $this -> reportHandler("1004", array('cookie_life_time', 'INTEGER'));
                                $errorCount++;
                            }
                            break;

                        case 'reroute':
                            $reroute = $settings['reroute'];
                            if(!is_bool($reroute)) {
                                $this -> reportHandler("1004", array('reroute', 'BOOLEAN'));
                                $errorCount++;
                            }

                            if ($usesGet == true && $reroute == true) {
                                $this -> reportHandler("2003");
                            }
                            break;

                        //default:
//                               /$this -> reportHandler("1002", $optionalParameter);
                        //return false;


                    }
                    }
                }


            }

            $this -> settingsVerified = true;

            if ($errorCount > 0) {

                return false;

            }

            else {

                return true;
        }


    }



    public function checkOptional(string $needle) {

        $settings = $this -> settings();

        if (array_key_exists($needle, $settings)) {
            return $settings[$needle];
        }

        else {
            return false;
        }

    }

    public function log(array $errors) {


        $path = $this -> checkOptional('log_path');
        $filename = $this -> checkOptional('log_file_name');
        $timestamp = $this -> getTimeDate('datetime');

        if ($path == false) {
            $path = $this -> getSetPath(null,'log');
        }

        else {

            //verify this
            if (substr($path, -1) != '/') {
                $path = $path.'/';
            }
        }

        if ($filename == false) {
            $filename = $this -> getSetFileName();
        }

        if (!file_exists($path)) {

            $chmod = $this -> getSetChmod();

            if (mkdir($path, $chmod, true)) {

                $this -> reportHandler('3000', array('Logging Mechanism', $path));

            }

            else {
                $this -> reportHandler('1007', array('Logging Mechanism', $path));
                return false;
            }



        }

        foreach ($errors as $error) {

            $error = explode(':', $error);

            $filePath = $path.$filename;

            $logLine = '['.$timestamp.'] ['.$error[0].']'.$error[1]. PHP_EOL;

            if (!file_put_contents($filePath, $logLine, FILE_APPEND | LOCK_EX)) {
                $this -> reportHandler('1008', array($filename));
                return false;
            }

        }


        return true;

    }

    /**
     * Run the settings
     *
     * @param bool $verifySettings;
     *
     * @return bool
     *
     */

    public function run(bool $verifySettings = true)
    {

        if ($verifySettings == true) {
            if ($this->settingsVerified == false) {
                if ($this->verifySettings() == false) {
                    return false;
                }
            }
        }

        $settings = $this -> settings();
        $languages = explode(',', str_replace(' ', '', $settings['languages']));


        $this -> getSetLanguages($languages);
        $this -> getSetDefaultLang($settings['default_language']);

        $useGet = $this -> checkOptional('use_get_handle');
        $useJSON = $this -> checkOptional('use_json');
        $mo = $settings['mo'];
        $getAlias = $this -> checkOptional('get_alias');
        $cookieLifeTime = $this->checkOptional('cookie_lifetime');
        $reroute = $this -> checkOptional('reroute');


        if ($useGet != false) {
            if ($getAlias != false) {
                $this -> getVariable($useGet);
                $this -> getAlias($getAlias);
            }

        }

        if ($cookieLifeTime != false) {
            $this -> getSetCookieLifetime($cookieLifeTime);
        }

        $useGet = $this -> getVariable();
        $getAlias = $this -> getAlias();
        $cookieLifeTime = $this -> getSetCookieLifetime();
        $cookie = $this -> checkCookie();

        if ($cookie != false) {

            if (isset($_GET[$getAlias]) AND $useGet == true) {
                $this -> getMechanism($getAlias, $cookieLifeTime);
            } elseif ($reroute == true AND $this -> getHost() != true) {
                $this -> reroute($this -> getHost(), $cookie);
            } else {
                $this->getSetLanguage($cookie);
            }

                if ($useJSON == true) {

                    $jsonPath = $this -> checkOptional('json_path');

                    if ($jsonPath != false) {

                        $this -> getSetPath($jsonPath, 'json');

                    }

                    $this -> getSetLangPack($cookie);


            }


        } else {


            if ($mo == 'headers' && $this -> getHeaders() != false) {
                $details = $this -> getHeaders();

            } else {
                $details = $this -> getDetails($this->getIp());
            }

            $details = strtolower($details);
            $this -> language = $this -> setLanguage($details);
            $this -> setLangCookie($cookieLifeTime);

            if ($reroute == true AND $this -> getHost() != false) {

                $this -> reroute($this -> getHost(), $this -> getSetLanguage());

            } else {

                if ($useJSON == true) {

                    $jsonPath = $this -> checkOptional('json_path');

                    if ($jsonPath != false) {

                        $this -> getSetPath($jsonPath, 'json');

                    }

                    $this -> getSetLangPack($this -> getSetLanguage());
                }
            }

        }
    }


    public function localize(bool $verifySettings = true, bool $logErrors = true)
    {


        if ($verifySettings == true) {

            $verifiedSettings = $this -> verifySettings();

            if ($verifiedSettings != true) {

                if ($logErrors == true) {
                    $errors = $this -> reportHandler();
                    $this -> log($errors);
                }

                return false;

            }

        }

        $this -> run($verifySettings);

        if($logErrors == true) {

            $errors = $this -> reportHandler();

            if (is_array($errors)) {
                $this -> log($errors);
            }

        }



    }


    // =======================================================================//
    // ! Constructor                                                          //
    // =======================================================================//

    public function __construct(array $settings)
    {
        if (!empty($settings)) {
            $this -> settings($settings);
        }

        else {
            $this -> reportHandler('1000');
        }

    }
}

?>