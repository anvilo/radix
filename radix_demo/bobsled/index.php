
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/fav.png">
    <!-- Author Meta -->
    <meta name="author" content="CodePixar">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>Bobsled</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:300,500,600" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/lang_dropdown.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="main-wrapper-first">
    <div class="hero-area relative">
        <header>
            <div class="container">
                <div class="header-wrap">
                    <div class="header-top d-flex justify-content-between align-items-center">
                        <div class="logo">
                        </div>
                        <div class="main-menubar d-flex align-items-center">
                            <nav class="hide">
                                <a href="#start">Language detection</a>
                                <a href="#defaultl">Default language</a>
                                <a href="#languageOv">Language override</a>
                                <a href="#otherF">Other features</a>
                            </nav>
                            <div class="menu-bar"><span class="lnr lnr-menu"></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="banner-area">
            <div class="container">
                <div class="row height align-items-center">
                    <div class="col-lg-7">
                        <div class="banner-content">
                            <h1 class="text-white text-uppercase mb-10">Welcome to Radix 2.0.0 demo</h1>
                            <p class="text-white mb-30">This demo page is designed to demonstrate the Radix capabilites.</p>
                            <a href="#start" class="primary-btn d-inline-flex align-items-center"><span class="mr-10">Start here</span><span class="lnr lnr-arrow-right"></span></a>
                            <a href="https://bitbucket.org/anvilo/radix/src/master/" class="primary-btn d-inline-flex align-items-center"><span class="mr-10">View code on Bitbucket.</span><span class="lnr lnr-arrow-right"></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section id="start"></section>
<div class="main-wrapper">
    <div class="working-process-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center">
                        <h2>Detected language: Italian</h2>
                        <p>Radix has detected that your preferred browser language is <strong> Italian</strong>.</p>
                        <p>Below you may find the languages that were indicated as supported in the Radix settings by the system admin. Should Radix detect you visiting this page from any of these countries, the website content would be dynamically converted and translated:</p>

                    </div>
                </div>
            </div>
            <div class="total-work-process d-flex flex-wrap justify-content-around align-items-center">

                <div class='single-work-process'>
                    <div class='work-icon-box'>
                        <img class='flags' src='http://flagpedia.net/data/flags/normal/ru.png'' alt=''>
                    </div><h4 class='caption'>RU</h4></div><div class='single-work-process'>
                    <div class='work-icon-box'>
                        <img class='flags' src='http://flagpedia.net/data/flags/normal/it.png'' alt=''>
                    </div><h4 class='caption'> IT</h4></div><div class='single-work-process'>
                    <div class='work-icon-box'>
                        <img class='flags' src='http://flagpedia.net/data/flags/normal/de.png'' alt=''>
                    </div><h4 class='caption'> DE</h4></div><div class='single-work-process'>
                    <div class='work-icon-box'>
                        <img class='flags' src='http://flagpedia.net/data/flags/normal/es.png'' alt=''>
                    </div><h4 class='caption'> ES</h4></div>					</div>
        </div>
    </div>
</div>
<div class="main-wrapper">
    <!-- Start Feature Area -->
    <section class="featured-area" id="defaultl">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center">
                        <h2 class="text-white">Default language: EN</h2>
                        <p class="text-white">The chosen default language is set and displayed as the main website language. It is also used as a failsafe mechanism to ensure the content is always displayed.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="single-feature">
                        <div class="thumb" style="background: url(img/radix/1.jpg);"></div>
                        <div class="desc text-center mt-30">
                            <h4 class="text-white">1. Radix detects user location</h4>
                            <p class="text-white">Radix accurately detects user location or location preferences.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-feature">
                        <div class="thumb" style="background: url(img/radix/2.png);"></div>
                        <div class="desc text-center mt-30">
                            <h4 class="text-white">2. Radix checks if the website has a localization support for the detected location</h4>
                            <p class="text-white">If that is the case,  Radix will present a translated website, specific currency, shipping expenses etc. </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-feature">
                        <div class="thumb" style="background: url(img/radix/3.png);"></div>
                        <div class="desc text-center mt-30">
                            <h4 class="text-white">3. Otherwise, a default language is set</h4>
                            <p class="text-white">A default language is set for every user that doesn't match any of the supported locations by the website. Usually the default language is the primary language.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>



    </section>
    <section class="remarkable-area" id="languageOv">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center">

                        <h2>Language override</h2>
                        <p>Give users power to override automatic language settings to something they prefer with a simple URL query string control. Radix will remember this setting by using cookies.</p>
                        <p>In this demo, the $_GET handle that triggers language override is set to be <strong>: lg</strong></p>
                        <p>Language override: (Check the "Detected Language" section after making a selection)</p>

                        <ul class="languagepicker roundborders large">

                            <li class="note">Hover here for override options</li>

                            <a href='http://localhost/radix_demo/bobsled/index.php?lg=ru'><li><img src='http://flagpedia.net/data/flags/normal/ru.png''/>Russian</li></a><a href='http://localhost/radix_demo/bobsled/index.php?lg=it'><li><img src='http://flagpedia.net/data/flags/normal/it.png''/>Italian</li></a><a href='http://localhost/radix_demo/bobsled/index.php?lg=de'><li><img src='http://flagpedia.net/data/flags/normal/de.png''/>German</li></a><a href='http://localhost/radix_demo/bobsled/index.php?lg=es'><li><img src='http://flagpedia.net/data/flags/normal/es.png''/>Spanish; Castilian</li></a>                                </ul>
                    </div>
                </div>
            </div>

    </section>

    <!-- End Feature Area -->
    <!-- Start Remarkable Wroks Area -->
    <section class="remarkable-area-below" id="otherF">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="section-title text-center">
                        <h2>Other Features</h2>
                        <p>Radix is packed with other useful features</p>
                    </div>
                </div>
            </div>
            <div class="single-remark">
                <div class="row no-gutters">
                    <div class="col-lg-7 col-md-6">
                        <div class="remark-thumb" style="background: url(img/r1.jpg);"></div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <div class="remark-desc">
                            <h4>Error catching & handling</h4>
                            <p>Catch and fetch Radix specific errors in a controlled manner. Choose where and when to read errors by using a dedicated error fetching method.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-remark">
                <div class="row no-gutters">
                    <div class="col-lg-7 col-md-6">
                        <div class="remark-desc">
                            <h4>Built-in error logger</h4>
                            <p>Have your errors, notices and warnings logged on a separate .txt file.</p>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <div class="remark-thumb" style="background: url(img/logs.png);"></div>
                    </div>
                </div>
            </div>
            <div class="single-remark">
                <div class="row no-gutters">
                    <div class="col-lg-7 col-md-6">
                        <div class="remark-thumb" style="background: url(img/r3.jpg);"></div>
                    </div>
                    <div class="col-lg-5 col-md-6">
                        <div class="remark-desc">
                            <h4>Remember user settings with customizable cookies</h4>
                            <p>Radix allows you to customize and set cookies to remember your users' preferences.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Remarkable Wroks Area -->
    <!-- Start Story Area -->
    <section class="story-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-1">
                </div>
                <div class="col-lg-7">
                    <div class="story-box">
                        <h6 class="text-uppercase">Radix is designed to be injectable</h6>
                        <p>Radix allows easy injection into your development environment and can function as an extension or a part of your whole project. Each new instance of Radix can be configured with its own set of settings and instructions whether it is a localized email sending service, shipping services with calculated expediture prices or a whole localized website.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Story Area -->



    <!-- End Subscription Area -->
    <!-- Start Contact Form -->
    <section class="contact-form-area">
        <div class="container">
            <div class="row justify-content-center">
                <p class="col-lg-6">
                    <div class="section-title text-center">
                        <h2 class="text-white">Error handling and fetching</h2>
                <p class="text-white">By default Radix errors are being collected and logged but not publicly displayed. You can error retrieval by calling the retrieveErrors(ERROR_LEVEL) method:</p>
                <p class="text-green">No errors were found</p>
                <p class="text-white">ERROR_LEVEL setting allows you to choose what level errors you wish to report.</p>



            </div>
        </div>
</div>
</div>
</section>


<section class="contact-form-area">
    <div class="container">
        <div class="row justify-content-center">
            <p class="col-lg-6">
                <div class="section-title text-center">
                    <h2 class="text-white">Error logging</h2>
            <p class="text-white">Additionally, errors can be stored and logged into a file using the log() method. </p>



        </div>
    </div>
    </div>
    </div>
</section>

</div>




<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>
