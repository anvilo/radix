<?php

require_once '../radix/radix.class.php';

//iso639-1

$json = file_get_contents('../radix/iso639-1.json');

function jsonDecode($json, $assoc = false)
{
    $ret = json_decode($json, $assoc);
    if ($error = json_last_error())
    {
        $errorReference = [
            JSON_ERROR_DEPTH => 'The maximum stack depth has been exceeded.',
            JSON_ERROR_STATE_MISMATCH => 'Invalid or malformed JSON.',
            JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded.',
            JSON_ERROR_SYNTAX => 'Syntax error.',
            JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded.',
            JSON_ERROR_RECURSION => 'One or more recursive references in the value to be encoded.',
            JSON_ERROR_INF_OR_NAN => 'One or more NAN or INF values in the value to be encoded.',
            JSON_ERROR_UNSUPPORTED_TYPE => 'A value of a type that cannot be encoded was given.',
        ];
        $errStr = isset($errorReference[$error]) ? $errorReference[$error] : "Unknown error ($error)";
        throw new \Exception("JSON decode error ($error): $errStr");
    }
    return $ret;
}

//Decode JSON

$iso = jsonDecode($json, true);



$settings = array(

    //REQUIRED PARAMS
    'languages' => 'RU, IT, DE, ES',
    'default_language' => 'EN',

    //OPTIONAL PARAMS
    'use_get_handle' => true,
    'get_alias' => 'lg', //alternatives: null - Radix doesn't act on the retrieved information, reroute - radix uses the traditional reroute method: example.com/en
    'file_path' => '/',
    'mo' => 'headers',
    'log_path' => 'logs/',
    'use_json' => true,
    'json_path' => 'lang_files/',
    'log_errors' => true, //Note: if log_path is not explicitly indicated this setting will be ignored
    'cookie_life_time' => 20//days

);




require_once 'includes/header.html';


$radix = new Radix($settings);

$localize = $radix -> localize(true, true);

$language = strtoupper($radix -> getSetLanguage());

$defaultLanguage = $radix -> getSetDefaultLang();


?>

